<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator as Faker;

use App\Models\Wallet;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Wallet::class, function (Faker $faker) {
    return [
        'id' => $faker->unique()->randomNumber,
        'user_id' => $faker->unique()->randomNumber,
        'balance' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = NULL),
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s'),
    ];
});
