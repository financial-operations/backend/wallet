# Microservice Wallet

Microsserviço responsável por realizar operações na carteira (saldo em conta) dos usuários.

### Settings:
- [Lumen Framework - Versão 7.2.1](https://lumen.laravel.com/docs/7.x)
- [PHP - Versão 7.4.4](https://www.php.net/downloads.php)

### Requirements:
- [Lumen Framework](https://lumen.laravel.com/docs/7.x/installation#server-requirements)

### Endpoint microservice Wallet.

- http://localhost:porta/v1/wallet


### Installation and Configuration:

- Clone projeto
        
       `git clone git@gitlab.com:financial-operations/backend/wallet.git`

- Imagem docker
        
        `registry.gitlab.com/financial-operations/backend/wallet`
        
### Executar somente o microservice Wallet.

- 1) Fazer o clone do projeto:
        
        `git clone git@gitlab.com:financial-operations/backend/wallet.git`

- 2) Acessar o diretório wallet e executar os seguintes comandos e/ou configurações:
        
        configurar o arquivo .env
        Instalar dependências do projeto com: `composer install`

- 3) Subir a aplicação em um container docker (troque o nome home_user pelo diretório do projeto):
        
        docker container run -dit --memory="256m" --name orq-wallet -h ms-wallet -v ~/home_user/wallet:/var/www -p 9000?80 registry.gitlab.com/financial-operations/backend/wallet:latest && docker container exec orq-wallet service apache2 start && docker container exec orq-wallet service apache2 status


