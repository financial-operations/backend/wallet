<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['middleware' => 'api.auth', 'prefix' => 'v1', 'namespace' => 'Api\v1'], function () use ($router) {
    $router->get('wallet', 'WalletController@index');
    $router->get('wallet/{walletId}', 'WalletController@show');
    $router->post('wallet', 'WalletController@store');
    $router->put('wallet/{walletId}', 'WalletController@update');
    $router->delete('wallet', 'WalletController@destroy');
});
