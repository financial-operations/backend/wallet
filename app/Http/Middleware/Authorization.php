<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Http\Response;

use App\Services\GuzzleService;

class Authorization
{
    private $guzzleService;

    public function __construct(GuzzleService $guzzleService)
    {
        $this->guzzleService = $guzzleService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $response = $this->guzzleService->makeConnection('post', 'check_token', env('SSO_URL').'checktoken', $request->BearerToken());
        if ($response == Response::HTTP_OK || $response == Response::HTTP_ACCEPTED) {
            return $next($request);
        }
        
        return response()->json(['error' => 'Unauthorized access parando aqui no middleware authorization da wallet!'], Response::HTTP_NON_AUTHORITATIVE_INFORMATION);
    }
}
