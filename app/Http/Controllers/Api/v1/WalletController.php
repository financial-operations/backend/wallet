<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Services\WalletService;

class WalletController extends Controller
{
    private $walletService;

    public function __construct(WalletService $walletService)
    {
        $this->walletService = $walletService;
    }

    /**
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        try {
            return response()->json($this->walletService->getAllData(), Response::HTTP_OK);
        } catch (\Exception $ex) {
            return response()->json(['error' => 'Uhm... isso não parece bom! Não foi possível recuperar os dados da carteira, tente novamente mais tarde.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param int $walletId
     * @return Illuminate\Http\Response
     */
    public function show($walletId)
    {
        try {
            return response()->json($this->walletService->findWalletById($walletId), Response::HTTP_OK);
        } catch (\Exception $ex) {
            return response()->json(['error' => 'Uhm... isso não parece bom! Não foi possível recuperar a carteira, tente novamente mais tarde.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            return response()->json($this->walletService->newWallet($request->all()), Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            return response()->json(['error' => 'Uhm... isso não parece bom! Não foi possível inserir uma nova carteira, tente novamente mais tarde.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, $walletId)
    {
        try {
            return response()->json($this->walletService->updateWalletBalance($request->all(), $walletId), Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            return response()->json(['error' => 'Uhm... isso não parece bom! Não foi possível atualizar o saldo da carteira, tente novamente mais tarde.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            return response()->json($this->walletService->removeWallet($request->id), Response::HTTP_OK);
        } catch (\Exception $ex) {
            return response()->json(['error' => 'Uhm... isso não parece bom! Não foi possível remover a carteira, tente novamente mais tarde.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}