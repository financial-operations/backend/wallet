<?php

namespace App\Services;

use App\Repositories\Contracts\WalletRepositoryInterface;

class WalletService
{
    private $formattedWallets;
    private $walletRepository;

    public function __construct(WalletRepositoryInterface $walletRepository)
    {
        $this->walletRepository = $walletRepository;
        $this->formattedWallets = [];
    }

    public function getAllData()
    {
        return $this->walletRepository->getAll();
    }

    public function newWallet(array $dataWallets)
    {
        return $this->walletRepository->storeCustomerWallet($dataWallets);
    }

    public function findWalletById($walletId)
    {
        return $this->walletRepository->findById(intval($walletId));
    }

    public function updateWalletBalance(array $transaction, $walletId)
    {
        switch ($transaction['transaction_type']) {
            case 1:
                return $this->walletRepository->incrementBalance($transaction['amount'], $walletId);
            case 2:
                return $this->walletRepository->decrementBalance($transaction['amount'], $walletId);
            default:
                return false;
        }
    }

    public function removeWallet($walletId)
    {
        return $this->walletRepository->delete($walletId);
    }
}