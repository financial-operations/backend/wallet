<?php

namespace App\Services;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Message\Response;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Client;

class GuzzleService
{
    private $guzzleHttpClient;

    public function __construct(Client $guzzleHttpclient)
    {
        $this->guzzleHttpclient = $guzzleHttpclient;
    }

    public function makeConnection(string $verb, string $typeRequest, string $url, $token = null, $body = null)
    {
        $header = [
            'Content-Type'  => 'application/json',
            'Authorization' => 'Bearer '.$token,
        ];

        switch ($typeRequest) {
            case 'check_token':
                return $this->guzzleHttpclient->request($verb, $url, $header)->getStatusCode();
            default:
                $response = $this->guzzleHttpclient->request($verb, $url,['header' => $header,\GuzzleHttp\RequestOptions::JSON => $body]);
                return json_decode($response->getBody()->getContents());
        }
    }
}