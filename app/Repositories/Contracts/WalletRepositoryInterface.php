<?php

namespace App\Repositories\Contracts;

interface WalletRepositoryInterface
{
    public function getAll();

    public function storeCustomerWallet(array $dataWallets);

    public function findById(int $walletId): Object;

    public function incrementBalance($amount, int $walletId);

    public function decrementBalance($amount, int $walletId);

    public function delete(int $walletId);
}