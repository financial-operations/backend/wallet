<?php 

namespace App\Repositories;

use Illuminate\Support\Facades\DB;

use App\Repositories\Contracts\WalletRepositoryInterface;

class WalletRepository implements WalletRepositoryInterface
{
    public function getAll()
    {
        return  DB::table('wallet as w')
                    ->select(
                        'w.id',
                        'w.user_id',
                        'w.balance',
                        DB::raw('DATE_FORMAT(w.created_at, "%d/%m/%Y %H:%i:%S") as created_at'),
                        DB::raw('DATE_FORMAT(w.updated_at, "%d/%m/%Y %H:%i:%S") as updated_at')
                    )
                    ->orderBy('w.updated_at', 'desc')
                    ->get();
    }

    public function storeCustomerWallet(array $dataWallets)
    {
        return DB::table('wallet')->insert($dataWallets);
    }

    public function findById(int $walletId): Object
    {
        return  DB::table('wallet as w')
                    ->where('w.id', '=', $walletId)
                    ->select(
                        'w.id',
                        'w.user_id',
                        'w.balance',
                        DB::raw('DATE_FORMAT(w.created_at, "%d/%m/%Y %H:%i:%S") as created_at'),
                        DB::raw('DATE_FORMAT(w.updated_at, "%d/%m/%Y %H:%i:%S") as updated_at')
                    )
                    ->first();
    }

    public function incrementBalance($amount, int $walletId)
    {
        return  DB::table('wallet as w')
                    ->where('w.id', '=', $walletId)
                    ->increment('balance', $amount, ['updated_at' => date('Y-m-d H:i:s')]);
    }

    public function decrementBalance($amount, int $walletId)
    {
        return  DB::table('wallet as w')
                    ->where('w.id', '=', $walletId)
                    ->decrement('balance', $amount, ['updated_at' => date('Y-m-d H:i:s')]);
    }

    public function delete(int $walletId)
    {
        return  DB::table('wallet as w')
                    ->where('w.id', '=', $walletId)
                    ->delete();
    }
}