<?php

use Faker\Generator as Faker;

use App\Models\Wallet;
use App\Repositories\WalletRepository;

class WalletTest extends TestCase
{
    private $wallet;
    private $walletRepository;

    public function setUp(): void
    {
        parent::setUp();
        
        $this->wallet = factory(Wallet::class)->make();
        $this->walletRepository = new WalletRepository();
    }

    /** @test*/
    public function should_return_all_wallets()
    {
        $wallets = $this->walletRepository->getAll();
        
        $this->assertInstanceOf(Illuminate\Support\Collection::class, $wallets);
    }

    /** @test */
    public function should_return_wallet()
    {
        $this->assertTrue(true);
    }

    /** @test */
    public function should_create_wallet()
    {
        $createdWallet = $this->walletRepository->storeCustomerWallet($this->wallet->toArray());
        
        $this->assertTrue($createdWallet);
    }
}
